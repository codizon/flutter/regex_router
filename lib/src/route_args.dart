import 'package:collection/collection.dart';

const Equality _equality = DeepCollectionEquality();

/// Route arguments. Contains [pathArgs] and [body].
/// See [RegexRouter] for example.
class RouteArgs {
  /// All path arguments. Empty map if no arguments were provided.
  final Map<String, String?> pathArgs;

  /// Object passed to [NavigatorState.push] or [NavigatorState.pushNamed] arguments parameter.
  /// Null if no additional argument was specified.
  final Object? body;

  /// Route arguments constructor.
  RouteArgs(this.pathArgs, [this.body]);

  /// Gets path argument from [pathArgs] by given [key].
  String? operator [](String key) => pathArgs[key];

  @override
  int get hashCode =>
      runtimeType.hashCode ^ _equality.hash(pathArgs) ^ _equality.hash(body);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RouteArgs &&
          runtimeType == other.runtimeType &&
          _equality.equals(pathArgs, other.pathArgs) &&
          _equality.equals(body, other.body);
}
