library regex_router;

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';

import 'src/route_args.dart';
export 'src/route_args.dart';

/// Route builder function. See [RegexRouter] for usage sample.
typedef Widget RouteBuilder(BuildContext context, RouteArgs args);

/// {@template regex_router}
/// Router with regex syntax.
///
/// For each path, you can use [RegExp] pattern. All named groups are passed
/// to [pathArgs] parameter (see [RegExpMatch]). Moreover, there's an option to build
/// simple argument that matches `[a-zA-Z0-9_\\\-\.,:;\+*^%\$@!]+` regex. To do so, just put a group name
/// straight after a colon (group name must fit `[a-zA-Z0-9_-]+` pattern):
///
/// Both examples are equal:
///
/// ```dart
/// "/:id": (context, args) => Page(args["id"])
/// "/(?<id>[a-zA-Z0-9_-]+)" => Page(args["id"])
/// ```
///
/// To start using [RegexRouter], create it's instance using [RegexRouter.create] factory method:
///
/// ```dart
/// final router = RegexRouter.create({
///   "/": (context, _) => HomePage(),
///   "/second/:id/": (context, args) => AnotherPage(id: args["id"]),
///   "/withBody": (context, args) => PageWithBody(body: args.body),
/// });
/// ```
///
/// And then attach it to your Material's app:
///
/// ```dart
/// MaterialApp(
///   // ...
///   onGenerateRoute: router.generateRoute,
///   initialRoute: "/",
///   // ...
/// )
/// ```
///
/// To navigate, just use a named route:
/// ```dart
/// Navigator.of(context).pushNamed("/")
/// Navigator.of(context).pushNamed("/second/7")
/// Navigator.of(context).pushNamed("/withBody", arguments: {"key": "value"})
/// ```
/// {@endtemplate}
abstract class RegexRouter {
  /// A function to be attached in [MaterialApp.onGenerateRoute].
  Route<dynamic>? generateRoute(RouteSettings settings);

  /// A factory method to create [RegexRouter] instance with [routeMap].
  factory RegexRouter.create(Map<String, RouteBuilder> routeMap) {
    return _RegexRouterImpl(routeMap);
  }
}

class _RegexRouterImpl implements RegexRouter {
  final List<_RouteEntry> _routeRegexMap;

  _RegexRouterImpl(Map<String, RouteBuilder> routeMap)
      : _routeRegexMap = <_RouteEntry>[
          for (var key in routeMap.keys) _buildRouteEntry(key, routeMap[key]!),
        ];

  @override
  Route? generateRoute(RouteSettings settings) {
    final routeName = _cleanRouteName(settings.name!);

    RegExpMatch? match;
    final routeEntry = _routeRegexMap.firstWhereOrNull((it) {
      match =
          it.regex.firstMatch(routeName) ?? it.regex.firstMatch("$routeName/");
      return match != null;
    });

    if (routeEntry == null) return null;

    List<String> names;

    if (match != null &&
        match!.groupCount > 0 &&
        match!.groupNames.isNotEmpty) {
      names = match!.groupNames.toList();
    } else {
      names = [];
    }

    final pathArgs = <String, String?>{
      for (var name in names) name: match?.namedGroup(name),
    };

    return MaterialPageRoute(
      settings: settings,
      builder: (context) => routeEntry.routeBuilder(
        context,
        RouteArgs(pathArgs, settings.arguments),
      ),
    );
  }
}

class _RouteEntry {
  final String name;
  final RegExp regex;
  final RouteBuilder routeBuilder;

  _RouteEntry(this.name, this.regex, this.routeBuilder);

  @override
  int get hashCode => runtimeType.hashCode ^ name.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _RouteEntry &&
          runtimeType == other.runtimeType &&
          name == other.name;
}

_RouteEntry _buildRouteEntry(String name, RouteBuilder routeBuilder) {
  final cleanRouteName = _cleanRouteName(name);
  final variableRegex = '[a-zA-Z0-9_-]+';
  final nameWithParameters = cleanRouteName.replaceAllMapped(
    RegExp(":($variableRegex)"),
    (match) {
      final groupName = match.group(1);
      return "(?<$groupName>[a-zA-Z0-9_\\\-\.,:;\+*^%\$@!]+)";
    },
  );
  final regex = RegExp("^$nameWithParameters\$", caseSensitive: false);
  return _RouteEntry(name, regex, routeBuilder);
}

String _cleanRouteName(String name) {
  name = name.trim();
  final parts = name.split("/");
  parts.removeWhere((value) => value == "");
  parts.map((value) {
    if (value.startsWith(":")) {
      return value;
    } else {
      return value.toLowerCase();
    }
  });
  name = parts.join("/");
  return name;
}
